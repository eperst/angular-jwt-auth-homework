import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { JwtModule } from '@auth0/angular-jwt';
import { JWTconfig } from 'src/environments/environment';
import { TokenInterceptor } from './token.interceptor';
import { CarouselComponent } from './home/carousel/carousel.component';
import { TableComponent } from './home/table/table.component';

@NgModule({
  declarations: [AppComponent, HomeComponent, ProfileComponent, LoginComponent, CarouselComponent, TableComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: JWTconfig
    }),
    NgBootstrapFormValidationModule.forRoot(),
    NgBootstrapFormValidationModule
  ],
  providers: [
    {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true,
    }
  ],
  bootstrap: [AppComponent],
  exports: [],
})
export class AppModule {}
