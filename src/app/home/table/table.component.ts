import { Component, PipeTransform, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { CATS, Cat } from '../../interfaces/interfaces';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers: [DecimalPipe]
})
export class TableComponent implements OnInit {
  page = 1;
  pageSize = 4;
  collectionSize = CATS.length;
  filteredCats: Cat[];
  filter = new FormControl('');

  constructor(private pipe: DecimalPipe) { }

  // filters all records by search term
   ngOnInit() {
    this.filter.valueChanges.pipe(
      startWith(''),
      map(text => search(text, this.pipe))).subscribe(res => {
        this.filteredCats = res as Cat[];
      });
   }

  // pagination implemented on filtered array
  get cats(): Cat[] {
    this.collectionSize = this.filteredCats.length;
    return this.filteredCats
      .map((cat, i) => ({id: i + 1, ...cat}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }
}

// search implemented on initial data
function search(text: string, pipe: PipeTransform): Cat[] {
  return CATS.filter(cat => {
    const term = text.toLowerCase();
    return cat.breed.toLowerCase().includes(term)
        || cat.country.toLowerCase().includes(term)
        || cat.origin.toLowerCase().includes(term)
        || cat.coat.toLowerCase().includes(term)
        || cat.pattern.toLowerCase().includes(term);
  });
}



