import { Component } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent {
  images = [`http://placekitten.com/500/600`, `http://placekitten.com/500/400`, `http://placekitten.com/800/700`];
  // array of cat images in carousel
}
