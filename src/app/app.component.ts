import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './services/login.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-jwt-auth-homework';
  constructor(private router: Router, private login: LoginService, private http: HttpClient) {}

  // returns true if profile and logout button should be visible
  visible(): boolean {
    return this.login.loggedIn;
  }

  // runs if logout button is clicked
  logout() {
    this.login.logout();
  }
}
