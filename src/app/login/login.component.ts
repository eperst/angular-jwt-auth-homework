import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { tap } from 'rxjs/operators';
import { AuthResponse } from '../interfaces/interfaces';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  token: AuthResponse;
  subscriptions: Subscription[] = [];
  constructor(private fb: FormBuilder, private loginService: LoginService) { }

  // pre-checks login input data with regex
  ngOnInit() {
    this.loginForm = this.fb.group({
        mail: ['', [Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required]],
        password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  // calls the login service and saves the returned token
  onSubmit() {
    this.subscriptions.push(this.loginService
      .login({
        ...this.loginForm.value,
      })
      .pipe(tap(res => {
        localStorage.setItem('access_token', res.token);
      })).subscribe());
  }

  // subscription cleanup
  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
