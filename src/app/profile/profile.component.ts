import { Component } from '@angular/core';
import { LoginService } from '../services/login.service';

@Component({
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  contents = 'undefined';
  constructor(private login: LoginService) { }

  // gets current status from login service
  getStatus(): string {
    return this.login.contents;
  }

}
