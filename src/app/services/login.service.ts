import { Injectable, OnDestroy } from '@angular/core';
import { AppLogin, StatusResponse } from '../interfaces/interfaces';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Subscription } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoginService implements OnDestroy {
  subscriptions: Subscription[] = [];
  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }
  contents: string;
  first: boolean;
  helper = new JwtHelperService();

  // sends user data to /login
  login(body: AppLogin) {
    this.first = true;
    return this.http.post<{token: string}>(`${this.apiUrl}login`, body);
  }

  // returns current token
  public getToken(): string {
    return localStorage.getItem('access_token');
  }

  // fetches current status from /status/authenticated endpoint
  private authStatus() {
    this.http.get<StatusResponse>(`${this.apiUrl}status/authenticated`).pipe(tap(res => {
        this.contents = res.status;
    })).subscribe();
  }

  // checks if user is logged in and token is not expired and fetches status on every new login
  public get loggedIn(): boolean {
    const token = this.getToken();
    if (token !== null) {
      if (this.helper.isTokenExpired(token)) {
        this.logout();
        return false;
      } else {
        if (this.first) {
          this.authStatus();
          this.first = false;
        }
        return true;
      }
    }
    return false;
  }

  // removes token
  public logout() {
    this.first = true;
    localStorage.removeItem('access_token');
  }

  // subscription cleanup
  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
