import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private login: LoginService, private router: Router) { }

  // is activated when user is logged in
  canActivate(): boolean {
    if (!this.login.loggedIn) {
      this.router.navigate(['home']); // redirect to home
      return false;
    }
    return true;
  }
}
